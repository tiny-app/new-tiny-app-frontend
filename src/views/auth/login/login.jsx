import React, { useState, useEffect } from 'react';
import {
  Container,
  Row,
  Col,
} from 'react-bootstrap';

import {
  Form,
  Button,
  Input,
  Icon
} from 'antd';

import { useStore } from 'stores/context.js';
import AuthService from "components/authService.js";

const Login = props => {

  const auth = new AuthService();

  const [ state, dispatch ] = useStore();

  const [ isSubmit, setIsSubmit ] = useState(false);

  useEffect(() => {
    auth.isLoggedIn()
    .then(() => {
      props.history.push('/admin/index');
    })
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const { getFieldDecorator } = props.form;
  
  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        setIsSubmit(true);
        auth.login(values.username, values.password)
        .then(res => {
          props.history.push('/admin/index')
        })
        .catch(() => {
          props.form.setFields({
            username: {
              value: values.username,
              errors: []
            },
            password: {
              value: '',
              errors: [
                new Error('Username or password is incorrect!')
              ]
            }
          })         
        })
        .finally(() => {
          setIsSubmit(false);
        })
      }
    });
  };

  return (
    <Container fluid>
      <Row>
        <Col
          xs="12"
          lg="6"
          className="d-flex align-items-center justify-content-center"
          style={{ height: '100vh' }}
        >
          <div style={{
            width: '70%'
          }}>
            <img
              src={require('assets/img/logo/logo.svg')}
              alt="TS"
              style={{
                marginLeft: '-5px',
                width: '250px',
              }}
            />
            <div className="big-text" style={{ marginTop: '70px', marginBottom: '50px' }}>
              <h2>Login</h2>
              <p className="text-muted">login first to entering application</p>
            </div>
            <Form onSubmit={handleSubmit}>
              <Form.Item>
                {getFieldDecorator('username', {
                  rules: [{ required: true, message: 'Please input your username!' }],
                })(
                  <Input
                    placeholder="Username"
                    suffix={
                      <Icon type="user" style={{ color: 'rgba(0,0,0,.45)' }} />
                    }
                  />
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('password', {
                  rules: [{ required: true, message: 'Please input your password!' }],
                })(
                  <Input.Password
                    placeholder="Password"
                  />
                )}
              </Form.Item>
              <Form.Item>
                <Button id="btn-submit" type="primary" htmlType="submit" loading={isSubmit}>
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Col>    
        <Col
          lg="6"
          style={{
            height: '100vh',
            background: 'linear-gradient(33.59deg, #283997 -6.86%, #00F6FA 97.14%)'
          }}
          className="d-none align-items-center justify-content-center d-lg-flex"
        >
          <img
            src={require('assets/img/lock.svg')}
            alt="Lock"
            style={{
              width: '45%'
            }}
          />
        </Col>
      </Row>
    </Container>
  )
}

export default Form.create({ name: 'login' })(Login)