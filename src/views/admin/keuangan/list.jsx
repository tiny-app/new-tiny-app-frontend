import React, { useState, useEffect } from 'react';
import {
  Col,
  Row
} from 'react-bootstrap';

import {
  Table,
  Breadcrumb,
  Button,
  Tag,
  Dropdown,
  Icon,
  Menu,
  message,
  Modal,
} from 'antd';

import { Card } from 'react-bootstrap';

import Form from './form';

import { formatNum } from 'components/helper.js';
import axios from 'customAxios.js';
import moment from 'moment';
import db from 'components/db.js';

export default (() => {

  const [ modalStatus, setModalStatus ] = useState(false);
  const [ data, setData ] = useState([]);
  const [ params, setParams ] = useState({});
  const [ pagination, setPagination ] = useState({});
  const [ doFetch, setDoFetch ] = useState(true);
  const [ isLoaded, setIsLoaded ] = useState(false);
  const [ editData, setEditData ] = useState(null);
  const [ fullImage, setFullImage ] = useState({
    no: null,
    img: null
  });
  const { confirm } = Modal;

  const handleModal = () => {
    if(modalStatus && editData) {
      setEditData(null);
    }
    setModalStatus(!modalStatus)
  }

  useEffect(() => {
    const fetchData = () => {
      setIsLoaded(false)
      setDoFetch(false)
      axios.get(`/finance/transaction`, {
        params: {
          page: pagination.current,
        }
      })
      .then(res => {
        setData(res.data.data.data)
        setIsLoaded(true)
        setPagination({
          current: res.data.data.pagination.current_page,
          total: res.data.data.pagination.total
        })
        db.list.put({ name: 'transaction', value: res.data.data.data })
      })
      .catch(async () => {
        const data = await db.list.get('transaction');
        setData(data.value)
        setIsLoaded(true)
        setPagination({
          current: 1,
          total: 10
        })
      })
    }
    if(doFetch) fetchData()
  }, [pagination, doFetch])

  const submitForm = (type) => {
    setPagination({
      current: !editData && type !== 'delete' ? 1 : pagination.current
    })
    setDoFetch(true)
  }

  const handleTableChange = (pPagination, filters, sorter) => {
    setDoFetch(true)
    setPagination({
      ...pagination,
      current: pPagination.current
    })
  };

  const deleteData = (id, no) => {
    confirm({
      title: 'Yakin ingin menghapus data tersebut?',
      content: `Transaksi #${no}`,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        deleteProcess()
      },
    });
    const deleteProcess = () => {
      const key = `delete-${id}`
      message.loading({
        content: `Transaksi #${no} hapus loading...`,
        key
      });
      axios.delete(`/finance/transaction/${id}`)
      .then(() => {
        message.success({
          content: `Transaksi #${no} berhasil dihapus!`,
          key
        })
        submitForm('delete');
      })
      .catch(() => {
        message.error({
          content: `Transaksi #${no} gagal dihapus!`,
          key
        })
      })
    }
  }

  const columns = [
    {
      title: 'No.',
      dataIndex: 'no_transaction',
      key: 'no_transaction',
      render: no => `#${no}`
    },
    {
      title: 'Jenis',
      dataIndex: 'type.name',
      key: 'type',
      render: tag => {
        let color;
        if(tag === 'Pemasukan') {
          color = 'blue';
        } else if(tag === 'Pengeluaran' || tag === 'Piutang') {
          color = 'volcano';
        } else color = 'gold'
        return <Tag color={color}>{tag}</Tag>
      }
    },
    {
      title: 'Transaksi',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Jumlah',
      dataIndex: 'amount_formatted',
      key: 'amount',
      render: val => 'Rp. ' + formatNum(val)
    },
    {
      title: 'Tanggal',
      dataIndex: 'date',
      key: 'date',
      render: date => moment(date).format('DD MMMM YYYY HH:mm:ss')
    },
    {
      title: 'Oleh',
      dataIndex: 'user.name',
      key: 'user',
      render: tag => <Tag color='blue'>{tag}</Tag>
    },
    {
      title: 'Gambar',
      dataIndex: 'file_url',
      key: 'file',
      render: (gbr, data) => (
        gbr
          ? <img 
              src={gbr}
              alt="gambar"
              style={{ width: '100px', cursor: 'pointer' }}
              onClick={() => setFullImage({ no: data.no_transaction, img: gbr })}
            />
          : 'No image!'
      )
    },
    {
      title: '',
      render: (col, row) => {
        const menu = (
          <Menu>
            <Menu.Item onClick={() => {
              setEditData(row)
              handleModal()
            }}>
              <Icon type="edit" />
              Edit
            </Menu.Item>
            <Menu.Item onClick={() => deleteData(row.id, row.no_transaction)}>
              <Icon type="delete" />
              Delete
            </Menu.Item>
          </Menu>
        );

        return (
          <Dropdown overlay={menu} trigger={['click']} placement="bottomRight">
            <Button shape="circle" type="primary" icon="more" />
          </Dropdown>
        )
      }
    }
  ]

  return (
    <>
      <Row className="mb-3">
        <Col>
          <h2 className="m-0">Keuangan</h2>
          <Breadcrumb>
            <Breadcrumb.Item>Keuangan</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
          </Breadcrumb>
        </Col>
        <Col className="d-flex align-items-center justify-content-end">
          <Button type="primary" onClick={handleModal}>
            <Icon type="plus" /> Tambah
          </Button>
        </Col>
      </Row>
      <Row>
        <Col>
          <Card className="border">
            <Table
              tableLayout="auto"
              rowKey="id"
              dataSource={data}
              onChange={handleTableChange}
              pagination={pagination}
              columns={columns}
              loading={!isLoaded}
              showSizeChanger={true}
              scroll={{ x: 1000 }}
            />
          </Card>
        </Col>
      </Row>
      <Form
        visible={modalStatus}
        handleModal={handleModal}
        submitForm={submitForm}
        data={editData}
      />
      <Modal
        title={`Full image of #${fullImage.no}`}
        visible={fullImage.no}
        footer={null}
        width="80%"
        onCancel={() => setFullImage({ no: null, img: null})}
      >
        <img src={fullImage.img} alt="fullimage" width="100%" />
      </Modal>
    </>
  )
})