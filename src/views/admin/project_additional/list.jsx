import React, { useState, useEffect } from 'react';
import {
  Col,
  Row
} from 'react-bootstrap';

import {
  Table,
  Breadcrumb,
  Button,
  Tag,
  Dropdown,
  Icon,
  Menu,
  message,
  Modal,
} from 'antd';

import { Card } from 'react-bootstrap';

import Form from './form';
import axios from 'customAxios.js';

export default (() => {

  const [ modalStatus, setModalStatus ] = useState(false);
  const [ data, setData ] = useState([]);
  const [ params, setParams ] = useState({});
  const [ pagination, setPagination ] = useState({});
  const [ doFetch, setDoFetch ] = useState(true);
  const [ isLoaded, setIsLoaded ] = useState(false);
  const [ editData, setEditData ] = useState(null);
  const { confirm } = Modal;

  const handleModal = () => {
    if(modalStatus && editData) {
      setEditData(null);
    }
    setModalStatus(!modalStatus)
  }

  useEffect(() => {
    const fetchData = () => {
      setIsLoaded(false)
      setDoFetch(false)
      axios.get(`/profile/project_additionals`, {
        params: {
          page: pagination.current,
        }
      })
      .then(res => {
        setData(res.data.data.data)
        setIsLoaded(true)
        setPagination({
          current: res.data.data.pagination.current_page,
          total: res.data.data.pagination.total
        })
      })
    }
    if(doFetch) {
      fetchData()
    };
  }, [pagination, doFetch])

  const submitForm = (type) => {
    setPagination({
      current: !editData && type !== 'delete' ? 1 : pagination.current
    })
    setDoFetch(true)
  }

  const handleTableChange = (pPagination, filters, sorter) => {
    setDoFetch(true)
    setPagination({
      ...pagination,
      current: pPagination.current
    })
  };

  const deleteData = (id, {name, type}) => {
    confirm({
      title: 'Yakin ingin menghapus data tersebut?',
      content: `Project Additional "${name} - ${type}"`,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        deleteProcess()
      },
    });
    const deleteProcess = () => {
      const key = `delete-${id}`
      message.loading({
        content: `Project Additional "${name} - ${type}" hapus loading...`,
        key
      });
      axios.delete(`/profile/project_additionals/${id}`)
      .then(() => {
        message.success({
          content: `Project Additional "${name} - ${type}" berhasil dihapus!`,
          key
        })
        submitForm('delete');
      })
      .catch(() => {
        message.error({
          content: `Project Additional "${name} - ${type}" gagal dihapus!`,
          key
        })
      })
    }
  }

  const columns = [
    {
      title: 'Nama',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Type',
      dataIndex: 'type_formatted',
      key: 'type_formatted',
      render: tag => {
        let color;
        if(tag === 'Language') {
          color = 'blue';
        } else if(tag === 'Framework') {
          color = 'volcano';
        } else color = 'gold'
        return <Tag color={color}>{tag}</Tag>
      }
    },
    {
      title: '',
      align: 'center',
      render: (col, row) => {
        const menu = (
          <Menu>
            <Menu.Item onClick={() => {
              setEditData(row)
              handleModal()
            }}>
              <Icon type="edit" />
              Edit
            </Menu.Item>
            <Menu.Item onClick={() => deleteData(row.id, {name: row.name, type: row.type_formatted})}>
              <Icon type="delete" />
              Delete
            </Menu.Item>
          </Menu>
        );

        return (
          <Dropdown overlay={menu} trigger={['click']} placement="bottomRight">
            <Button shape="circle" type="primary" icon="more" />
          </Dropdown>
        )
      }
    }
  ]

  return (
    <>
      <Row className="mb-3">
        <Col>
          <h2 className="m-0">Project Additional</h2>
          <Breadcrumb>
            <Breadcrumb.Item>Project Additional</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
          </Breadcrumb>
        </Col>
        <Col className="d-flex align-items-center justify-content-end">
          <Button type="primary" onClick={handleModal}>
            <Icon type="plus" /> Tambah
          </Button>
        </Col>
      </Row>
      <Row>
        <Col>
          <Card className="border">
            <Table
              tableLayout="auto"
              rowKey="id"
              dataSource={data}
              onChange={handleTableChange}
              pagination={pagination}
              columns={columns}
              loading={!isLoaded}
              showSizeChanger={true}
              scroll={{ x: 1000 }}
            />
          </Card>
        </Col>
      </Row>
      <Form
        visible={modalStatus}
        handleModal={handleModal}
        submitForm={submitForm}
        data={editData}
      />
    </>
  )
})