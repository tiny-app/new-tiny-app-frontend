import React, { useState, useEffect } from 'react';
import {
  Modal,
  Button,
  Form,
  Input,
  InputNumber,
  Select,
  DatePicker,
  message,
  Upload,
  Icon,
} from 'antd';
import axios from 'customAxios.js';
import moment from 'moment';

const Forms = (props) => {

  const { getFieldDecorator } = props.form;

  const [ formState, setFormState ] = useState({
    name: '',
    type: undefined,
  });

  useEffect(() => {
    if(props.data) {
      setFormState({
        name: props.data.name,
        type: props.data.type,
      })
    }
    else {
      setFormState({
        name: '',
        type: undefined,
      })
    }
  }, [props.data])

  // const isChanged = ( name, newVal ) => formState[name] !== newVal ? true : false

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        setFormState({ ...formState, isSubmit: true })
        const formData = new FormData();
        formData.append('name', values.name)
        formData.append('type', values.type)
        if(props.data) formData.append('_method', 'put')
        axios({
          method: 'post',
          url: !props.data ? `/profile/project_additionals` : `/profile/project_additionals/${props.data.id}`,
          data: formData,
        })
        .then(res => {
          resetForm();
          const msg = !props.data ? 'ditambahkan' : 'diubah';
          message.success(`Project additional berhasil ${msg}!`)
          props.submitForm();
          if(props.data) props.handleModal();
        })
        .finally(() => setFormState({ ...formState, isSubmit: false }))
      }
    })
  }

  const handleModal = () => {
    resetForm();
    props.handleModal();
  }

  const resetForm = () => {
    props.form.resetFields(['name', 'type'])
  }

  const setFieldValue = (name, value) => {
    props.form.setFieldsValue({[name]: value});
  }

  return (
    <Modal
      visible={props.visible}
      title={!props.data ? 'Tambah Project Additional' : 'Edit Project Additional'}
      onCancel={handleModal}
      onOk={handleSubmit}
      footer={[
        <Button key="back" onClick={handleModal}>
          Cancel
        </Button>,
        <Button key="submit" type="primary" loading={formState.isSubmit} onClick={handleSubmit}>
          {
            !props.data ? 'Submit' : 'Update'
          }
        </Button>,
      ]}
    >
      <Form onSubmit={handleSubmit} layout="vertical">
        <Form.Item label="Type">
          {getFieldDecorator('type', {
            initialValue: formState.type,
            rules: [{ required: true, message: 'Pilih type!' }],
          })(
            <Select
              placeholder="Pilih type"
              style={{ width: '100%' }}
              allowClear
              onSelect={value => setFieldValue('type', value)}
            >
              <Select.Option key={1} value="1">Language</Select.Option>
              <Select.Option key={2} value="2">Framework</Select.Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Nama">
          {getFieldDecorator('name', {
            initialValue: formState.name,
            rules: [{ required: true, message: 'Masukkan nama!' }],
          })(
            <Input
              placeholder="Masukkan nama"
              onChange={e => setFieldValue('name', e.target.value)}
            />
          )}
        </Form.Item>
      </Form>
    </Modal>
  )
}

export default Form.create({ name: 'form' })(Forms)