import React from 'react';
import { Switch, Route } from 'react-router-dom';
import list from './list';

export default (() => (
  <Switch>
    <Route exact path="/admin/project-additional" component={list} />
  </Switch>
))