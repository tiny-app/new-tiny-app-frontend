import React, { useState, useEffect } from 'react';
import {
  Modal,
  Button,
  Form,
  Input,
  InputNumber,
  Select,
  DatePicker,
  message,
  Upload,
  Icon,
} from 'antd';
import axios from 'customAxios.js';
import moment from 'moment';

const Forms = (props) => {

  const { getFieldDecorator } = props.form;

  const [ transactionType, setTransactionType ] = useState({
    isLoaded: false,
    data: [],
  });

  const [ formState, setFormState ] = useState({
    name: '',
    harga: '',
    tanggal: null,
    jenis: undefined,
    image: undefined,
    imagePreview: undefined,
    isSubmit: false,
    isUploadLoading: false,
  });

  useEffect(() => {
    if(props.data) {
      setFormState({
        name: props.data.name,
        harga: Math.abs(props.data.amount),
        tanggal: props.data.date ? moment(props.data.date, 'YYYY-MM-DD HH:mm') : null,
        jenis: props.data.type_id,
        imagePreview: props.data.file,
        isSubmit: false
      })
    }
  }, [props.data])

  useEffect(() => {
    const fetchTransactionType = () => {
      setTransactionType({
        isLoaded: false
      })
      axios.get(`/finance/transaction-type`, {
        params: {
          is_pending: 1
        }
      })
      .then(res => {
        setTransactionType({
          isLoaded: true,
          data: res.data.data.data,
        })
      })
    }
    fetchTransactionType();
  }, []);

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        setFormState({ ...formState, isSubmit: true })
        const formData = new FormData();
        formData.append('name', values.name)
        if(values.harga) formData.append('amount', values.harga)
        if(values.tanggal) formData.append('date', moment(values.tanggal).format('YYYY-MM-DD HH:mm'))
        formData.append('type_id', values.jenis)
        if(values.image) formData.append('image', values.image)
        if(props.data) formData.append('_method', 'PATCH')
        axios({
          method: 'post',
          url: !props.data 
               ? `/finance/transaction`
               : props.type === 'validate'
                 ? `/finance/transaction/done/${props.data.id}`
                 :`/finance/transaction/${props.data.id}`,
          data: formData,
          params: {
            pending: 1
          },
          headers: { 'Content-Type': 'multipart/form-data' }
        })
        .then(res => {
          resetForm();
          const msg = !props.data ? 'ditambahkan' : 'diubah';
          message.success(`Transaksi${props.data ? ` #${props.data.no_transaction} ` : ' terbaru '}berhasil ${msg}!`)
          props.submitForm();
          if(props.data) props.handleModal();
        })
        .finally(() => setFormState({ ...formState, isSubmit: false }))
      }
    })
  }

  const handleModal = () => {
    resetForm();
    props.handleModal();
  }

  const resetForm = () => {
    props.form.resetFields(['name', 'harga', 'tanggal', 'jenis', 'image'])
    setFormState({
      ...formState,
      imagePreview: undefined
    })
  }

  const setFieldValue = (name, value) => {
    props.form.setFieldsValue({[name]: value});
  }
  
  function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
  }

  const uploadButton = (
    <div>
      <Icon type={formState.isUploadLoading ? 'loading' : 'plus'} />
      <div className="ant-upload-text">Upload</div>
    </div>
  )

  const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  const handleChangeImage = (info, type) => {
    if(type === 'delete') {
      setFormState({
        ...formState,
        imagePreview: undefined,
      })
      setFieldValue('image', undefined)
    }
    else {
      if (info.file.status === 'uploading') {
        setFormState({
          ...formState,
          isUploadLoading: true
        })
        setFieldValue('image', undefined)
        return;
      }
  
      if (info.file.status === 'done') {
        getBase64(info.file.originFileObj, imageUrl => {
          setFormState({
            ...formState,
            imagePreview: imageUrl,
            isUploadLoading: false
          })
          setFieldValue('image', info.file.originFileObj)
        })
      }
    }
  }

  return (
    <Modal
      visible={props.visible}
      title={!props.data
             ? 'Tambah Transaksi'
             : props.type === 'validate'
               ? 'Validasi Transaksi'
               : 'Edit Transaksi'}
      onCancel={handleModal}
      onOk={handleSubmit}
      footer={[
        <Button key="back" onClick={handleModal}>
          Cancel
        </Button>,
        <Button key="submit" type="primary" loading={formState.isSubmit} onClick={handleSubmit}>
          {
            !props.data ? 'Submit' : 'Update'
          }
        </Button>,
      ]}
    >
      <Form onSubmit={handleSubmit} layout="vertical">
        <Form.Item label="Jenis">
          {getFieldDecorator('jenis', {
            initialValue: formState.jenis,
            rules: [{ required: true, message: 'Pilih jenis transaksi!' }],
          })(
            <Select
              placeholder="Pilih jenis transaksi"
              style={{ width: '100%' }}
              allowClear
              loading={!transactionType.isLoaded}
              disabled={!transactionType.isLoaded}
              onSelect={value => setFieldValue('jenis', value)}
            >
              {
                transactionType.isLoaded && transactionType.data.map((dt, index) => (
                  <Select.Option key={index} value={dt.id}>{dt.name}</Select.Option>
                ))
              }
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Transaksi">
          {getFieldDecorator('name', {
            initialValue: formState.name,
            rules: [{ required: true, message: 'Masukkan nama transaksi!' }],
          })(
            <Input
              placeholder="Masukkan nama transaksi"
              onChange={e => setFieldValue('name', e.target.value)}
            />
          )}
        </Form.Item>
        <Form.Item label="Harga">
          {getFieldDecorator('harga', {
            initialValue: formState.harga,
            rules: [{ required: props.type === 'validate' ? true : false, message: 'Masukkan harga!' }],
          })(
            <InputNumber
              placeholder="Masukkan harga"
              style={{ width: '100%' }}
              formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              parser={value => value.replace(/\$\s?|(,*)/g, '')}
              onChange={value => setFieldValue('harga', value)}
            />
          )}
        </Form.Item>
        <Form.Item label="Tanggal & Waktu">
          {getFieldDecorator('tanggal', {
            initialValue: formState.tanggal,
            rules: [{ required: props.type === 'validate' ? true : false, message: 'Pilih tanggal!' }],
          })(
            <DatePicker
              style={{ width: '100%' }}
              showTime
              placeholder="Masukkan tanggal & waktu"
              onChange={value => setFieldValue('tanggal', value)}
              format="DD MMMM YYYY HH:mm:ss"
            />
          )}
        </Form.Item>
        <Form.Item label="Bukti Gambar">
          {getFieldDecorator('image', {
            getValueFromEvent: formState.image,
          })(
            <>
            <Upload
              name="gambar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              beforeUpload={beforeUpload}
              onChange={handleChangeImage}
              customRequest={({ onSuccess }) => setTimeout(() => onSuccess('ok'), 0)}
            >
              {formState.imagePreview ? <img src={formState.imagePreview} alt="gambar" style={{ width: '100%' }} /> : uploadButton}
            </Upload>
            { formState.imagePreview && <Button icon="delete" type="danger" onClick={(info) => handleChangeImage(info, 'delete')}>Delete</Button> }
            </>
          )}
        </Form.Item>
      </Form>
    </Modal>
  )
}

export default Form.create({ name: 'form' })(Forms)