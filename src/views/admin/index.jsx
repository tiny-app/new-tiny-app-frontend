import React, { useState, useEffect } from 'react';
import Card from 'components/dashboard/card';
import {
  Row,
  Col
} from 'react-bootstrap';
import { formatNum } from 'components/helper';
import axios from 'customAxios.js';
import db from 'components/db.js';

export default (() => {

  const [ state, setState ] = useState({
    saldo: 0,
    pemasukan: 0,
    pengeluaran: 0,
    targetPemasukan: 0,
    targetPengeluaran: 0
  })

  useEffect(() => {
    axios.get('/finance/transaction/sum_amount')
    .then(res => {
      const data = res.data.data;
      setState({
        saldo: data.balance,
        pemasukan: data.pemasukan,
        pengeluaran: data.pengeluaran,
        targetPemasukan: data.target_pemasukan,
        targetPengeluaran: data.target_pengeluaran
      })
      db.sum_amount.put({ name: 'saldo', value: data.balance })
      db.sum_amount.put({ name: 'pemasukan', value: data.pemasukan })
      db.sum_amount.put({ name: 'pengeluaran', value: data.pengeluaran })
      db.sum_amount.put({ name: 'targetPemasukan', value: data.target_pemasukan })
      db.sum_amount.put({ name: 'targetPengeluaran', value: data.target_pengeluaran })
    })
    .catch(async() => {
      let newState = {};
      await db.sum_amount.each(({ name, value }) => newState = {...newState, [name]: value})
      setState(newState)
    })
  }, [])

  return (
    <Row>
      <Col xs="12" md="6" lg="4" className="mb-3">
        <Card name="Saldo" value={formatNum(state.saldo)} icon="money" />
      </Col>
      <Col xs="12" md="6" lg="4" className="mb-3">
        <Card name="Pemasukan" value={formatNum(state.pemasukan)} icon="plus" />
      </Col>
      <Col xs="12" md="6" lg="4" className="mb-3">
        <Card name="Pengeluaran" value={formatNum(state.pengeluaran)} icon="minus" />
      </Col>
      <Col xs="12" md="6" lg="4" className="mb-3">
        <Card name="Target Pemasukan" value={formatNum(state.targetPemasukan)} icon="plus" />
      </Col>
      <Col xs="12" md="6" lg="4" className="mb-3">
        <Card name="Target Pengeluaran" value={formatNum(state.targetPengeluaran)} icon="minus" />
      </Col>
    </Row>
  )
})