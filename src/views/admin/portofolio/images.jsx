import React, { useState, useEffect, useCallback } from 'react';
import {
  Form,
  Button,
  Modal,
  Upload,
  Icon,
  Breadcrumb,
  message,
  Spin,
} from 'antd';
import axios from 'customAxios.js';
import {
  Row,
  Col,
  Card,
} from 'react-bootstrap';

const Forms = (props) => {

  const id = props.match.params.id;

  const [ formState, setFormState ] = useState({
    previewVisible: false,
    previewImage: '',
    fileList: [],
    isLoaded: false,
  });

  useEffect(() => {
    const fetchData = () => {
      axios.get(`/profile/project/files/${id}`)
      .then(res => {
        const data = res.data.data;
        let fileList = []
        data.forEach(dt => {
          fileList = [
            ...fileList,
            {
              uid: dt.id,
              name: dt.file,
              status: 'done',
              url: dt.file_url
            }
          ]
        })
        setFormState({
          ...formState,
          isLoaded: true,
          fileList
        })
      })
    }
    if(!formState.isLoaded) fetchData();
  }, [formState, id])

  function getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  const handleCancel = () => setFormState({...formState, previewVisible: false});

  const handlePreview = async file => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setFormState({
      ...formState,
      previewImage: file.url || file.preview,
      previewVisible: true,
    })
  };

  const handleChange = ({ file, fileList }) => {
    if(file.status === 'uploading') {
      const key = `upload-${file.uid}`
      message.loading({
        content: `Uploading image...`,
        key
      });
      const formData = new FormData();
      formData.append('image', file.originFileObj)
      axios.post(`/profile/project/files/${id}`, formData)
      .then(res => {
        file.status = 'done';
        file.uid = res.data.data.id;
        setFormState({...formState, fileList})
        message.success({
          content: `Upload success!`,
          key
        })
        console.log(res)
      })
    }
    else if(file.status === 'removed') {
      const key = `delete-${file.uid}`
      message.loading({
        content: `Deleting image...`,
        key
      });
      axios.delete(`/profile/project/files/${file.uid}`)
      .then(res => {
        setFormState({...formState, fileList})
        message.success({
          content: `Delete success!`,
          key
        })
      })
    }
  };

  const uploadButton = (
    <div>
      <Icon type="plus" />
      <div className="ant-upload-text">Upload</div>
    </div>
  );

  return (
    <>
      <Row className="mb-3">
        <Col>
          <h2 className="m-0">Portofolio</h2>
          <Breadcrumb>
            <Breadcrumb.Item>Portofolio</Breadcrumb.Item>
            <Breadcrumb.Item>Images</Breadcrumb.Item>
            <Breadcrumb.Item>{id}</Breadcrumb.Item>
          </Breadcrumb>
        </Col>
      </Row>
      <Row>
        <Col>
          <Card className="border p-4">
          {
            !formState.isLoaded
            ? <Spin tip="Loading..." />
            : <Upload
                customRequest={({ onSuccess }) => setTimeout(() => onSuccess('ok'), 0)}
                listType="picture-card"
                fileList={formState.fileList}
                onPreview={handlePreview}
                onChange={handleChange}
              >
                {!formState.isLoaded ? null : uploadButton}
              </Upload>
          }
          </Card>
        </Col>
      </Row>
      <Modal visible={formState.previewVisible} footer={null} onCancel={handleCancel}>
        <img alt="example" style={{ width: '100%' }} src={formState.previewImage} />
      </Modal>
    </>
  )
}

export default Form.create({ name: 'form' })(Forms)