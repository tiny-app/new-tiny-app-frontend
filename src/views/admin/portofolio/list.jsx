import React, { useState, useEffect } from 'react';
import {
  Col,
  Row,
  Card,
} from 'react-bootstrap';

import {
  Table,
  Breadcrumb,
  Button,
  Dropdown,
  Icon,
  Menu,
  message,
  Modal,
} from 'antd';
import axios from 'customAxios.js';

import {
  Link
} from 'react-router-dom';

export default (() => {

  const [ data, setData ] = useState([]);
  const [ params, setParams ] = useState({});
  const [ pagination, setPagination ] = useState({});
  const [ doFetch, setDoFetch ] = useState(true);
  const [ isLoaded, setIsLoaded ] = useState(false);
  const { confirm } = Modal;

  useEffect(() => {
    const fetchData = () => {
      setIsLoaded(false)
      setDoFetch(false)
      axios.get(`/profile/project`, {
        params: {
          page: pagination.current,
        }
      })
      .then(res => {
        setData(res.data.data.data)
        setIsLoaded(true)
        setPagination({
          current: res.data.data.pagination.current_page,
          total: res.data.data.pagination.total
        })
      })
    }
    if(doFetch) {
      fetchData()
    };
  }, [pagination, doFetch])

  const submitForm = (type) => {
    setPagination({
      current: type !== 'delete' ? 1 : pagination.current
    })
    setDoFetch(true)
  }

  const handleTableChange = (pPagination, filters, sorter) => {
    setDoFetch(true)
    setPagination({
      ...pagination,
      current: pPagination.current
    })
  };

  const deleteData = (id, title) => {
    confirm({
      title: 'Yakin ingin menghapus data tersebut?',
      content: `Project "${title}"`,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        deleteProcess()
      },
    });
    const deleteProcess = () => {
      const key = `delete-${id}`
      message.loading({
        content: `Project "${title}" hapus loading...`,
        key
      });
      axios.delete(`/profile/project/${id}`)
      .then(() => {
        message.success({
          content: `Project "${title}" berhasil dihapus!`,
          key
        })
        submitForm('delete');
      })
      .catch(() => {
        message.error({
          content: `Project "${title}" gagal dihapus!`,
          key
        })
      })
    }
  }

  const columns = [
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      render: desc => <p className="simple-desc">{desc}</p>
    },
    {
      title: 'Thumbnail',
      dataIndex: 'thumbnail_url',
      key: 'thumbnail',
      render: (gbr, data) => (
        gbr
          ? <img 
              src={gbr}
              alt="gambar"
              style={{ width: '100px' }}
            />
          : 'No image!'
      )
    },
    {
      title: '',
      render: (col, row) => {
        const menu = (
          <Menu>
            <Menu.Item>
              <Link to={`/admin/portofolio/images/${row.id}`}>
                <Icon type="file-image" className="mr-2" />Images
              </Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`/admin/portofolio/form/${row.id}`}>
                <Icon type="edit" className="mr-2" />Edit
              </Link>
            </Menu.Item>
            <Menu.Item onClick={() => deleteData(row.id, row.title)}>
              <Icon type="delete" className="mr-2" />Delete
            </Menu.Item>
          </Menu>
        );

        return (
          <Dropdown overlay={menu} trigger={['click']} placement="bottomRight">
            <Button shape="circle" type="primary" icon="more" />
          </Dropdown>
        )
      }
    }
  ]

  return (
    <>
      <Row className="mb-3">
        <Col>
          <h2 className="m-0">Portofolio</h2>
          <Breadcrumb>
            <Breadcrumb.Item>Portofolio</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
          </Breadcrumb>
        </Col>
        <Col className="d-flex align-items-center justify-content-end">
          <Link to="/admin/portofolio/form">
            <Button type="primary">
              <Icon type="plus" /> Tambah
            </Button>
          </Link>
        </Col>
      </Row>
      <Row>
        <Col>
          <Card className="border">
            <Table
              tableLayout="auto"
              rowKey="id"
              dataSource={data}
              onChange={handleTableChange}
              pagination={pagination}
              columns={columns}
              loading={!isLoaded}
              showSizeChanger={true}
              scroll={{ x: 1000 }}
            />
          </Card>
        </Col>
      </Row>
    </>
  )
})