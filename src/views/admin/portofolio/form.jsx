import React, { useState, useEffect, useCallback } from 'react';
import {
  Button,
  Form,
  Input,
  Select,
  message,
  Upload,
  Icon,
  Breadcrumb,
} from 'antd';
import axios from 'customAxios.js';
import {
  Row,
  Col,
  Card,
} from 'react-bootstrap';

const { TextArea } = Input;

const Forms = (props) => {

  const { getFieldDecorator } = props.form;

  const id = props.match.params.id;

  const [ technologies, setTechnologies ] = useState({
    isLoaded: false,
    data: [],
  });

  const [ formState, setFormState ] = useState({
    title: undefined,
    description: undefined,
    videoLink: undefined,
    demoLink: undefined,
    technologies: undefined,
    thumbnailName: undefined,
    thumbnailPreview: undefined,
    isLoaded: false,
    isSubmit: false,
  });

  useEffect(() => {
    const fetchTechnologies = () => {
      axios.get(`/profile/project_additionals`)
      .then(res => {
        setTechnologies({
          isLoaded: true,
          data: res.data.data.data,
        })
      })
    }
    fetchTechnologies();
  }, []);

  useEffect(() => {
    const fetchData = () => {
      axios.get(`/profile/project/${id}`)
      .then(res => {
        const data = res.data.data;
        let technologies = [];
        data.languages.forEach(({ project_additional_id: id }) => technologies = [...technologies, id])
        data.frameworks.forEach(({ project_additional_id: id }) => technologies = [...technologies, id])
        setFormState({
          title: data.title,
          description: data.description,
          videoLink: data.video_link,
          demoLink: data.demo_link,
          technologies: technologies,
          thumbnailName: undefined,
          thumbnailPreview: res.data.data.thumbnail_url,
          isLoaded: true,
          isSubmit: false,
        })
      })
    }
    if(id) fetchData();
  }, [id]);

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        setFormState({ ...formState, isSubmit: true })
        const formData = new FormData();
        formData.append('title', values.title)
        formData.append('description', values.description)
        if(values.demoLink) formData.append('demo_link', values.demoLink)
        if(values.videoLink) formData.append('video_link', values.videoLink)
        formData.append('technologies', JSON.stringify(values.technologies))
        if(values.thumbnailName) formData.append('thumbnail_name', values.thumbnailName.fileList[0].originFileObj, values.thumbnailName.fileList[0].name)
        if(id) formData.append('_method', 'PATCH')
        axios({
          method: 'post',
          url: !id ? `/profile/project` : `/profile/project/${id}`,
          data: formData,
          headers: { 'Content-Type': 'multipart/form-data' }
        })
        .then(res => {
          resetForm();
          const msg = !id ? 'ditambahkan' : 'diubah';
          message.success(`Project berhasil ${msg}!`)
        })
        .finally(() => setFormState({ ...formState, isSubmit: false }))
      }
    })
  }

  const resetForm = () => {
    props.form.resetFields()
    setFormState({
      ...formState,
      thumbnailPreview: undefined
    })
  }

  const fieldVal = props.form.getFieldsValue();

  const InputLoading = ({ loading }) => {
    if(loading) return <Icon type="loading" />
    else return null;
  }

  const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  return (
    <>
      <Row className="mb-3">
        <Col>
          <h2 className="m-0">Portofolio</h2>
          <Breadcrumb>
            <Breadcrumb.Item>Portofolio</Breadcrumb.Item>
            <Breadcrumb.Item>
              {
                !id ? 'Tambah' : 'Edit'
              }
            </Breadcrumb.Item>
            { id && <Breadcrumb.Item>{id}</Breadcrumb.Item> }
          </Breadcrumb>
        </Col>
      </Row>
      <Row>
        <Col>
          <Card className="border p-4">
            <Form onSubmit={handleSubmit} layout="vertical">
              <Form.Item label="Title">
                {getFieldDecorator('title', {
                  initialValue: formState.title,
                  rules: [{ required: true, message: 'Masukkan title!' }],
                })(
                  <Input
                    placeholder="Masukkan title"
                    disabled={id && !formState.isLoaded}
                    suffix={<InputLoading loading={id && !formState.isLoaded} />}
                  />
                )}
              </Form.Item>
              <Form.Item label="Description">
                {getFieldDecorator('description', {
                  initialValue: formState.description,
                  rules: [{ required: true, message: 'Masukkan description!' }],
                })(
                  <TextArea
                    rows={4}
                    placeholder="Masukkan description"
                    disabled={id && !formState.isLoaded}
                    suffix={<InputLoading loading={id && !formState.isLoaded} />}
                  />
                )}
              </Form.Item>
              <Row>
                <Col xs="12" lg="6">
                  <Form.Item label="Link demo">
                    {getFieldDecorator('demoLink', {
                      initialValue: formState.demoLink,
                    })(
                      <Input
                        placeholder="Masukkan link demo (opsional)"
                        disabled={id && !formState.isLoaded}
                        suffix={<InputLoading loading={id && !formState.isLoaded} />}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs="12" lg="6">
                  <Form.Item label="Link video">
                    {getFieldDecorator('videoLink', {
                      initialValue: formState.videoLink,
                    })(
                      <Input
                        placeholder="Masukkan link video (opsional)"
                        disabled={id && !formState.isLoaded}
                        suffix={<InputLoading loading={id && !formState.isLoaded} />}
                      />
                    )}
                  </Form.Item>
                </Col>
              </Row>
              <Form.Item label="Technologies">
                {getFieldDecorator('technologies', {
                  initialValue: formState.technologies,
                  rules: [{ required: true, message: 'Pilih teknologi! (min: 1)' }],
                })(
                  <Select
                    placeholder="Pilih teknologi"
                    style={{ width: '100%' }}
                    allowClear
                    loading={!technologies.isLoaded && id && !formState.isLoaded}
                    disabled={!technologies.isLoaded && id && !formState.isLoaded}
                    mode="multiple"
                    optionFilterProp="children"
                  >
                    {
                      technologies.isLoaded && technologies.data.map((dt, index) => (
                        <Select.Option
                          key={index} value={dt.id}
                        >
                          {dt.name} - {dt.type_formatted}
                        </Select.Option>
                      ))
                    }
                  </Select>
                )}
              </Form.Item>
              <Form.Item label="Thumbnail">
                {getFieldDecorator('thumbnailName', {
                  valuePropName: 'thumbnailName',
                  getValueFromEvent: formState.thumbnailName,
                  rules: [{ required: !id, message: 'Pilih thumbnail!' }],
                  onChange: val => {
                    getBase64(val.file.originFileObj, img => {
                      setFormState({
                        ...formState,
                        thumbnailPreview: img
                      })
                    })
                  } 
                })(
                  <Upload
                    accept=".jpg,.jpeg,.png"
                    listType="picture-card"
                    className="avatar-uploader"
                    showUploadList={false}
                    customRequest={({ onSuccess }) => setTimeout(() => onSuccess('ok'), 0)}
                    disabled={id && !formState.isLoaded}
                  >
                    {
                      formState.thumbnailPreview 
                      ? <img src={formState.thumbnailPreview} alt="gambar" className="mw-100" />
                      : <div disabled={true}>
                          <Icon type={id && !formState.isLoaded ? 'loading' : 'plus'} />
                          <div className="ant-upload-text">Upload</div>
                        </div>
                      }
                  </Upload>
                )}
              </Form.Item>
              <Form.Item className="text-right">
                <Button key="back" onClick={props.history.goBack} className="mr-2">
                  Cancel
                </Button>
                <Button key="submit" type="primary" loading={formState.isSubmit} onClick={handleSubmit}>
                  {
                    !props.data ? 'Submit' : 'Update'
                  }
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </Col>
      </Row>
    </>
  )
}

export default Form.create({ name: 'form' })(Forms)