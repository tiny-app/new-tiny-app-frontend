import React from 'react';
import { Switch, Route } from 'react-router-dom';
import list from './list';
import form from './form';
import images from './images';

export default function Index() {
  return (
    <>
      <Route exact path="/admin/portofolio" component={list} />
      <Route path="/admin/portofolio/form/:id?" component={form} />
      <Route path="/admin/portofolio/images/:id" component={images} />
    </>
  )
}