import React, { useState, useEffect } from 'react';
import {
  Modal,
  Button,
  Form,
  Input,
  Select,
  message,
  Upload,
  Icon,
} from 'antd';
import axios from 'customAxios.js';
import moment from 'moment';
import {
  Row,
  Col
} from 'react-bootstrap';

const { TextArea } = Input;

const Forms = (props) => {

  const { getFieldDecorator } = props.form;

  const [ technologies, setTechnologies ] = useState({
    isLoaded: false,
    data: [],
  });

  const [ formState, setFormState ] = useState({
    title: '',
    description: '',
    videoLink: '',
    demoLink: '',
    technologies: undefined,
    thumbnailName: undefined,
  });

  useEffect(() => {
    const fetchTechnologies = () => {
      setTechnologies({
        isLoaded: false
      })
      axios.get(`/profile/project_additionals`)
      .then(res => {
        setTechnologies({
          isLoaded: true,
          data: res.data.data.data,
        })
      })
    }
    fetchTechnologies();
  }, []);

  useEffect(() => {
    if(props.data) {
      setFormState({
        name: props.data.name,
        harga: Math.abs(props.data.amount),
        tanggal: moment(props.data.date, 'YYYY-MM-DD HH:mm'),
        jenis: props.data.type_id,
        imagePreview: props.data.file_url,
        isSubmit: false
      })
    }
    else {
      setFormState({
        title: '',
        description: '',
        videoLink: '',
        demoLink: '',
        technologies: undefined,
        thumbnailName: undefined,
      })
    }
  }, [props.data])

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      console.log(values)
      if (!err) {
        setFormState({ ...formState, isSubmit: true })
        const formData = new FormData();
        formData.append('title', values.title)
        formData.append('description', values.description)
        if(values.demoLink) formData.append('demo_link', values.demoLink)
        if(values.videoLink) formData.append('video_link', values.videoLink)
        formData.append('technologies', JSON.stringify(values.technologies))
        formData.append('thumbnail_name', values.thumbnailName.fileList[0].originFileObj, values.thumbnailName.fileList[0].name)
        axios({
          method: 'post',
          url: `/profile/project`,
          data: formData,
          headers: { 'Content-Type': 'multipart/form-data' }
        })
        .then(res => {
          resetForm();
          const msg = !props.data ? 'ditambahkan' : 'diubah';
          message.success(`Project berhasil ${msg}!`)
          props.submitForm();
          if(props.data) props.handleModal();
        })
        .finally(() => setFormState({ ...formState, isSubmit: false }))
      }
    })
  }

  const handleModal = () => {
    resetForm();
    props.handleModal();
  }

  const resetForm = () => {
    props.form.resetFields()
  }

  const fieldVal = props.form.getFieldsValue();

  return (
    <Modal
      width="80%"
      visible={props.visible}
      title={!props.data ? 'Tambah Portofolio' : 'Edit Portofolio'}
      onCancel={handleModal}
      onOk={handleSubmit}
      footer={[
        <Button key="back" onClick={handleModal}>
          Cancel
        </Button>,
        <Button key="submit" type="primary" loading={formState.isSubmit} onClick={handleSubmit}>
          {
            !props.data ? 'Submit' : 'Update'
          }
        </Button>,
      ]}
    >
      <Form onSubmit={handleSubmit} layout="vertical">
        <Form.Item label="Title">
          {getFieldDecorator('title', {
            initialValue: formState.title,
            rules: [{ required: true, message: 'Masukkan title!' }],
          })(
            <Input
              placeholder="Masukkan title"
            />
          )}
        </Form.Item>
        <Form.Item label="Description">
          {getFieldDecorator('description', {
            initialValue: formState.description,
            rules: [{ required: true, message: 'Masukkan description!' }],
          })(
            <TextArea
              rows={4}
              placeholder="Masukkan description"
            />
          )}
        </Form.Item>
        <Row>
          <Col xs="12" lg="6">
            <Form.Item label="Link demo">
              {getFieldDecorator('demoLink', {
                initialValue: formState.demoLink,
              })(
                <Input
                  placeholder="Masukkan link demo (opsional)"
                />
              )}
            </Form.Item>
          </Col>
          <Col xs="12" lg="6">
            <Form.Item label="Link video">
              {getFieldDecorator('videoLink', {
                initialValue: formState.videoLink,
              })(
                <Input
                  placeholder="Masukkan link video (opsional)"
                />
              )}
            </Form.Item>
          </Col>
        </Row>
        <Form.Item label="Technologies">
          {getFieldDecorator('technologies', {
            initialValue: formState.technologies,
            rules: [{ required: true, message: 'Pilih teknologi! (min: 1)' }],
          })(
            <Select
              placeholder="Pilih teknologi"
              style={{ width: '100%' }}
              allowClear
              loading={!technologies.isLoaded}
              disabled={!technologies.isLoaded}
              mode="multiple"
            >
              {
                technologies.isLoaded && technologies.data.map((dt, index) => (
                  <Select.Option key={index} value={dt.id}>{dt.name} - {dt.type_formatted}</Select.Option>
                ))
              }
            </Select>
          )}
        </Form.Item>
        <Form.Item label="Thumbnail">
          {getFieldDecorator('thumbnailName', {
            valuePropName: 'thumbnailName',
            getValueFromEvent: formState.thumbnailName,
            rules: [{ required: true, message: 'Pilih thumbnail!' }],
          })(
            <Upload
              accept=".jpg,.jpeg,.png"
              customRequest={({ onSuccess }) => setTimeout(() => onSuccess('ok'), 0)}
            >
              <Button disabled={fieldVal.thumbnailName && fieldVal.thumbnailName.fileList.length >= 1}>
                <Icon type="upload" /> Click to upload
              </Button>
            </Upload>
          )}
        </Form.Item>
      </Form>
    </Modal>
  )
}

export default Form.create({ name: 'form' })(Forms)