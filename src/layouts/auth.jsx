import React from 'react';
import { Route, Switch } from "react-router-dom";
import routes from 'routes.js';

export default (() => {

  const getRoutes = routes => (
    routes.map((prop, key) => {
      if(prop.layout === '/auth') {
        return (
          <Route
            key={key}
            path={prop.layout + prop.path}
            component={prop.component}
          />
        )
      } else return null;
    })
  )
  
  return (
    <Switch>
      {getRoutes(routes)}
    </Switch>
  )
})