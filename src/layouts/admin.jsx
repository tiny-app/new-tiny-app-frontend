import React, { useEffect, useState } from 'react';
import { Route, Switch } from "react-router-dom";
import routes from 'routes.js';
import Sidebar from 'components/layouts/sidebar';
import Header from 'components/layouts/header';
import {
  Container,
} from 'react-bootstrap';
import AuthService from "components/authService.js";
import { Spin } from 'antd';

export default ((props) => {
  const auth = new AuthService();

  const [ isLoggedIn, setIsLoggedIn ] = useState(false);

  useEffect(() => {
    auth.isLoggedIn()
    .then(() => {
      setIsLoggedIn(true)
    })
    .catch(() => {
      props.history.push('/auth/login');
    })
  }, [auth, props.history]);

  const getRoutes = routes => (
    routes.map((prop, key) => {
      if(prop.layout === '/admin' && !prop.hasOwnProperty('dropdown')) {
        return (
          <Route
            key={key}
            path={prop.layout + prop.path}
            component={prop.component}
          />
        )
      } 
      else if(prop.hasOwnProperty('dropdown')) {
        return prop.dropdown.map((dRoute, dIndex) => {
          return (
            <Route
              key={dIndex}
              path={prop.layout + dRoute.path}
              component={dRoute.component}
            />
          )
        })
      } else return null;
    })
  )
  
  if(isLoggedIn) {
    return (
      <div id="wrapper" className="d-flex">
        <Sidebar
          routes={routes}
        />
        <div id="page-content-wrapper">
          <Header />
          <Container fluid className="p-3">
            <Switch>
              {getRoutes(routes)}
            </Switch>
          </Container>
        </div>
      </div>
    )
  }
  else {
    return (
      <div style={{
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)'
      }}>
        <Spin size="large" />
      </div>
    )
  }
})