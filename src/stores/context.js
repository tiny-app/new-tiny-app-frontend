import React, { createContext, useContext, useReducer } from 'react';

const StoreContext = createContext();
const initialState = {
  name: null,
  isLoggedIn: false,
}

const reducer = (state, action) => {
  switch(action.type) {
    case 'increment':
      return {
        count: state.count + 1
      }
    case 'decrement':
      return {
        count: state.count - 1
      }
    default: return state;
  }
}

export const StoreProvider = ({ children }) => {
  const [ state, dispatch ] = useReducer(reducer, initialState);

  return (
    <StoreContext.Provider value={[state, dispatch]}>
      {children}
    </StoreContext.Provider>
  )
}

export const useStore = () => useContext(StoreContext);