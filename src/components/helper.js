import Dexie from 'dexie';

export const formatNum = (val) => {
  const nf = new Intl.NumberFormat('id-ID');
  return nf.format(val);
}

export const db = new Dexie('TSDatabase');