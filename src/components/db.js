import Dexie from 'dexie';

const db = new Dexie('TSDatabase');
db.version(1).stores({
  sum_amount: 'name, value',
  list: 'name, value',
  transaction_done: '++, type, value',
  transaction_pending: '++, type, value'
})

export default db;