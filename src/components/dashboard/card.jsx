import React from 'react';
import {
  Card
} from 'antd';
import {
  Row,
  Col
} from 'react-bootstrap';
import Icofont from 'react-icofont';

const DashCard = ({
  name,
  value,
  icon
}) => {
  let backgroundColor;
  switch(icon) {
    case 'money': backgroundColor = '#81D552'; break;
    case 'plus': backgroundColor = '#70bf42'; break;
    case 'minus': backgroundColor = '#ed0000'; break;
    default: console.log('wew')
  }
  return (
    <Card className="dash-card">
      <Row className="d-flex align-items-center">
        <Col>
          <span className="header">{name}</span>
          <span className="value">Rp. {value}</span>
        </Col>
        <Col className="text-right">
          <Icofont icon={icon} size="2" style={{
            color: 'white',
            backgroundColor: backgroundColor,
            padding: '10px',
            borderRadius: '100%'
          }}/>
        </Col>
      </Row>
    </Card>
  )
}

export default DashCard