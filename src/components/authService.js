import axios from 'customAxios.js';

export default class AuthService {
  login(username, password) {
    return axios.post(`login`, {
      username,
      password
    }).then(res => {
      this.setToken(res.data.data.token);
      return Promise.resolve(res.data);
    })
  }

  getProfile() {
    return axios.get(`profile`)
    .then(res => {
      return Promise.resolve(res.data);
    })
  }

  logout() {
    return axios.post(`logout`, {})
    .then(res => {
      this.removeToken();
      return Promise.resolve(res.data);
    })
  }

  isLoggedIn() {
    return new Promise((resolve, reject) => {
      axios.get(`is_login`)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        if(err.response.status !== 401 && this.getToken()) resolve()
        else reject(err);
      })
    })
  }

  setToken(token) {
    localStorage.setItem('token', token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  removeToken() {
    localStorage.clear();
  }
}