import React from 'react';
import {
  Nav,
  NavItem,
  NavDropdown,
  Accordion,
} from 'react-bootstrap';

import { NavLink } from 'react-router-dom';
import Icofont from 'react-icofont';

export default ((props) => {

  const { routes } = props;

  const createLinks = routes => (
    routes.map((route, index) => {
      if(route.layout === '/admin' && !route.hasOwnProperty('dropdown')) {
        return (
          <NavItem key={index} className="nav-normal">
            <Nav.Link as={NavLink} to={route.layout + route.path} activeClassName="is-active">
              <Icofont icon={route.icon} size="2" />
              {route.name}
            </Nav.Link>
          </NavItem>
        )
      } 
      else if(route.hasOwnProperty('dropdown')) {
        return (
          <div key={index} className="nav-dropdown">
            <Accordion.Toggle as={NavItem} eventKey={index}>
              <div className="nav-link">
                <Icofont icon={route.icon} size="2" />
                {route.name}
                <Icofont icon="stylish-down" className="ml-1" />
              </div>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey={index}>
            <>
            {
              route.dropdown.map((dRoute, dIndex) => (
                <NavItem key={dIndex}>
                  <Nav.Link
                    as={NavLink} to={route.layout + dRoute.path}
                    activeClassName="is-active" className="pl-4"
                  >
                    <span className="nav-dropdown-item" />
                    {dRoute.name}
                  </Nav.Link>
                </NavItem>
              ))
            }
            </>
            </Accordion.Collapse>
          </div>
        )
      } else return null;
    })
  )

  return (
    <div className="bg-white shadow-sm" id="sidebar-wrapper">
      <div className="sidebar-heading mb-4">
        <img src={require('assets/img/logo/logo.svg')} alt="logo" style={{ width: '150px' }} />
      </div>
      <Accordion className="list-group">
        {createLinks(routes)}
        
      </Accordion>
    </div>
  )
})