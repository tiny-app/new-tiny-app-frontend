import React, { useEffect } from 'react';
import Icofont from 'react-icofont';
import {
  Avatar,
  Dropdown,
  Menu,
  Icon,
  message,
} from 'antd';
import AuthService from "components/authService.js";
import { withRouter } from 'react-router-dom'

const Header = (props) => {

  const auth = new AuthService();

  const logout = (e) => {
    e.preventDefault();
    const key = 'logout';
    message.loading({
      key,
      content: `Logging out...`,
    });
    auth.logout()
    .then(res => {
      message.success({
        key,
        content: `Logged out!`,
      })
      props.history.push('/auth/login');
    });
  }

  const toggle = () => {
    const wrapper = document.getElementById('wrapper');
    wrapper.classList.toggle('toggled')
  }

  // useEffect(() => {
  //   const wrapper = document.getElementById('wrapper');
  //   const pageContent = document.getElementById('main-content')
  //   pageContent.addEventListener('click', (event) => {
  //     if(wrapper.classList.contains('toggled')) {
  //       toggle()
  //     }
  //   })
  // }, [])

  return (
    <nav className="navbar navbar-dark border-bottom">
      <Icofont icon="signal" onClick={toggle} size="1" id="sidebar-toggler" style={{ display: 'block' }} />
      <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
        <li className="nav-item d-flex align-items-center text-white">
          <Dropdown overlay={
            <Menu>
              <Menu.Item>
                <a target="_blank" rel="noopener noreferrer" href="#logout" onClick={logout}>
                  <Icon type="logout" className="mr-2" />
                  Logout
                </a>
              </Menu.Item>
            </Menu>
          } placement="bottomRight" trigger={['click']}>
            <div style={{ cursor: 'pointer' }}>
              <Avatar icon="user" className="mr-2" />
              User
              <Icofont icon="caret-down" className="ml-2" />
            </div>
          </Dropdown>
        </li>
      </ul>
    </nav>
  )
}

export default withRouter(Header)