import axios from 'axios';
import {
  notification
} from 'antd';

const fetchClient = () => {
  const defaultOptions = {
    baseURL: process.env.REACT_APP_API_URL,
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
    },
  };

  let instance = axios.create(defaultOptions);
  
  instance.interceptors.request.use(function (config) {
    const token = localStorage.getItem('token');
    config.headers.Authorization =  token ? `Bearer ${token}` : '';
    return config;
  });

  instance.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    if(error.response && error.response.status >= 500) {
      const status = error.response.status;
      notification.warning({
        message: `
          Something happen in backend-side
          ${status && ` [code: ${status}]`}
        `,
        description: 'Please contact your technical support for further information.',
        duration: 3,
      })
    }
    return Promise.reject(error);
  });

  return instance;
};

export default fetchClient();