// Auth
import Login from 'views/auth/login/login';

// Admin
import Index from 'views/admin/index';
import Keuangan from 'views/admin/keuangan/index';
import Target from 'views/admin/target/index';
import Portofolio from 'views/admin/portofolio/index';
import ProjectAdditional from 'views/admin/project_additional/index';

const routes = [
  // Auth
  {
    path: "/login",
    name: "Login",
    component: Login,
    layout: "/auth",
  },
  // Admin
  {
    path: "/index",
    name: "Dashboard",
    component: Index,
    layout: "/admin",
    icon: "dashboard-web"
  },
  {
    name: "Keuangan",
    layout: "/admin",
    icon: "wallet",
    dropdown: [
      {
        path: "/keuangan",
        name: "Keuangan",
        component: Keuangan,
      },
      {
        path: "/target",
        name: "Target",
        component: Target,
      },
    ]
  },
  {
    name: "Web Profile",
    layout: "/admin",
    icon: "attachment",
    dropdown: [
      {
        path: "/portofolio",
        name: "Portofolio",
        component: Portofolio,
      },
      {
        path: "/project-additional",
        name: "Project Additional",
        component: ProjectAdditional,
      }
    ]
  }
];

export default routes;